@extends('layouts.app')

@section('page-header')
    <div class="d-flex mt-2 mb-4 justify-content-between align-items-center text-light  bg-secondary rounded p-3 mb-2" >
                            Team Member Dashboard
                            <h4 class="mr-4"><i class=" mr-2 fas fa-wallet"></i>{{$member->points}}</h4>

    </div>

@endsection

@section('content')

<div class="row">
    <div class="col-xl-3 col-md-6">
        <div class="card bg-success text-white mb-4">
            <div class="card-body d-flex justify-content-between"><h5>Completed Tasks</h5><h5>{{auth()->user()->completedTasksCount()}}</h5></div>
            <div class="card-footer d-flex align-items-center justify-content-between">
            <a class="small text-white stretched-link" href="{{route('task.membertasks','completed')}}">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>


    <div class="col-xl-3 col-md-6">
        <div class="card bg-primary text-white mb-4">
            <div class="card-body d-flex justify-content-between"><h5>Assigned Tasks</h5><h5>{{auth()->user()->assignedTasksCount()}}</h5></div>
            <div class="card-footer d-flex align-items-center justify-content-between">
            <a class="small text-white stretched-link" href="{{route('task.membertasks','assigned')}}">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6">
        <div class="card bg-danger text-white mb-4">
            <div class="card-body d-flex justify-content-between"><h5>Failed Tasks</h5><h5>{{auth()->user()->failedTasksCount()}}</h5></div>
            <div class="card-footer d-flex align-items-center justify-content-between">
            <a class="small text-white stretched-link" href="{{route('task.membertasks','failed')}}">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>


    <div class="col-xl-3 col-md-6">
        <div class="card bg-warning text-white mb-4">
            <div class="card-body d-flex justify-content-between"><h5>Under Review Tasks</h5><h5>{{auth()->user()->underReviewTasksCount()}}</h5></div>
            <div class="card-footer d-flex align-items-center justify-content-between">
            <a class="small text-white stretched-link" href="{{route('task.membertasks','review')}}">View Details</a>
                <div class="small  text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>

</div>




<ol class="breadcrumb mb-4  bg-primary">
    <li class="breadcrumb-item active text-white">Your Tasks</li>
</ol>
@if($tasks->count() == 0)

    <h1 class="text-center">No Tasks Assigned To You </h1>
@else
<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">Title</th>
        <th scope="col">Status</th>
        <th scope="col">Priority</th>
        <th scope="col">Deadline</th>
        <th scope="col">ID</th>

        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
    @foreach($tasks as $task)
      <tr>

        <td class="{{$task->statusColor()}}">{{$task->title}}</td>
      <td class="{{$task->statusColor()}}">{{$task->status}}</td>
        <td class="{{$task->statusColor()}}">{!!$task->priority!!}</td>
        <td class="{{$task->statusColor()}}"> @if($task->deadline < now()){{$task->deadline}}  @else{{ $task->deadline->diffForHumans(['parts' => 4])}} @endif </td>
        <td class="{{$task->statusColor()}}"> {{$task->id}}</td>
        <td class="">
            <div class="d-flex">
                @can('applyActions',$task)
                <form action="{{route('task.update',$task)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="is_task_submit" value="true">
                    <input type="hidden" name="task_member" value="{{auth()->id()}}">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>

                </form>
                <form action="{{route('task.update',$task)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="is_task_giveup" value="true">
                    <input type="hidden" name="task_member" value="{{auth()->id()}}">
                    <button type="submit" class="btn btn-sm btn-danger ml-3">Give up</button>

                </form>

                @else


                  <p class="{{$task->statusColor()}}">{{$task->status}}</p>


                @endcan
            </div>
        </td>



        @endforeach

    </tbody>
  </table>

@endif

@endsection

