@extends('layouts.app')
@section('page-title','Create Task')
@section('page-level-styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@section('content')


<div class="row">
    <div class="mx-auto col-md-8">

    <form action="{{ route('task.update',$task) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Task Title</label>
              <input
                type="text"
                name="task-title"
                class="form-control"
                value="{{$task->title}}"
                disabled
                placeholder="title">
        </div>

        <div class="form-group">
            <label for="task-deadline">Task Deadline</label>
            <input type="date"
            class="form-control"
            name="task-deadline"
            placeholder="Click here to enter a deadline"
            id="task-deadline">

        </div>


        <div class="form-group">
            <label for="sel1">Select Severity</label>
            <select class="form-control" name="task-priority">
                {{-- <option disabled selected value> -- Select an Severity -- </option> --}}
                <option value=0 @if($task->priority === 0)  selected @endif>Medium</option>
                <option value=1 @if($task->priority === 1)  selected @endif>High</option>
                <option value=2 @if($task->priority === 2)  selected @endif>Highest</option>
            </select>
        </div>

        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div class="btn-group mr-2 mb-2" role="group" aria-label="First group">
              <button type="button" id="chooseButton" onclick="chooseButtonClicked()" class="btn btn-secondary">Choose Member Manually</button>

            </div>
            <div class="btn-group mr-2 mb-2" role="group" aria-label="Second group">
              <button type="button" onclick="automaticButtonClicked()" class="btn btn-secondary">Assign Task Automatically</button>

            </div>
        </div>

        <input type="hidden" id="is-automatic" name="is-automatic" value="false">

        <div class="form-group" id="choose-member">
            <label for="sel1">Select Member</label>
            <select class="form-control" name="task-member">
                <option disabled selected value> -- Select an Member-- </option>
                @foreach($members as $member)
                    @if(!$member->isLeader())
                      <option value="{{$member->id}}">{{$member->name}}</option>
                    @endif
                  @endforeach
            </select>
        </div>




        <button type="submit" class="btn btn-primary">Create</button>
    </form>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    var taskDeadline = @json($task->deadline->toDateTimeString());

      flatpickr("#task-deadline", {
            enableTime: true,
            minDate: 'today',
            allowInput: true,
            dateFormat: "Y-m-d H:i:ss",
            defaultDate: taskDeadline,

        });

        $chooseMemberSelect = document.getElementById('choose-member');
        $isAutomatic = document.getElementById('is-automatic');
        document.getElementById('chooseButton').click();

        function chooseButtonClicked()
        {
            console.log("jdbjd");
            $isAutomatic.value = "false";
            $chooseMemberSelect.classList.remove('d-none');
        }
        function automaticButtonClicked()
        {

            $isAutomatic.value = "true";
            $chooseMemberSelect.classList.add('d-none');
        }

</script>

@endsection
