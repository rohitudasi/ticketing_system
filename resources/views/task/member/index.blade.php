@extends('layouts.app')
@section('page-title','Tasks')
@section('content')

<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">Title</th>
        <th scope="col">Deadline</th>
      </tr>
    </thead>
    <tbody>
    @foreach($tasks as $task)
      <tr>
        <td>{{$task->title}}</td>
        <td> {{$task->deadline}}</td>

    @endforeach
    </tbody>
  </table>
@endsection
