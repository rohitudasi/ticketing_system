@extends('layouts.app')



@section('content')


<ol class="breadcrumb mb-4  bg-primary">
    <li class="breadcrumb-item active text-white">Team Tasks</li>
</ol>

<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">Title</th>
        <th scope="col">Assigned To</th>
        <th scope="col">Status</th>
        <th scope="col">Priority</th>
        <th scope="col">Deadline</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
    @foreach($tasks as $task)
      <tr>

        <td class="{{$task->statusColor()}}">{{$task->title}}</td>
        <td class="{{$task->statusColor()}}">{{$task->assignedTo()->name ??"NONE"}}</td>
        <td class="{{$task->statusColor()}}">{{$task->status}}</td>
        <td class="{{$task->statusColor()}}">{!!$task->priority!!}</td>
        <td class="{{$task->statusColor()}}"> @if($task->deadline < now()){{$task->deadline}}  @else{{ $task->deadline->diffForHumans(['parts' => 4])}} @endif </td>
        <td>
            <div class="d-flex">
                @if(auth()->user()->can('acceptOrReject',$task))

                <form id="accept_form" action="{{route('task.update',$task)}}" method="POST">
                    @csrf
                @method('PUT')
                    <input type="hidden" name="is_task_accept" value="true">
                    <input type="hidden" name="task_leader" value="{{auth()->id()}}">
                    <input type="hidden" id="input-points" name="task_points" value=1>


                </form>

                <button id="accept_button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Accept</button>
                <form action="{{route('task.update',$task)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="is_task_reassign" value="true">
                    <button type="submit" class="btn btn-sm ml-2 btn-danger">Re-assign</button>
                </form>


                @elseif(auth()->user()->can('assign',$task))

                <a href="{{route('task.assign',$task)}}" class="btn btn-primary btn-sm">Assign</a>


                @else

                       <p class="{{$task->statusColor()}}">{{$task->status}}</p>
                @endcan


            </div>
        </td>

    @endforeach
    </tbody>
  </table>


  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Submit Ratings</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container">
                <div id="rating-component"></div>
              </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button"  onclick='acceptButtonHandler()' class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>


@endsection

@section('page-level-scripts')



<script>

        $isSelected = false;
        $selectedStar = 0;
        $ratingComponent = document.getElementById('rating-component');
        $ratingComponent.classList.add('d-flex','justify-content-center');
        for(i=0;i<3;i++)
        {
            $starContainer = document.createElement('div');
            $starContainer.classList.add('ml-3','star-container');
            //creating star
            $star = document.createElement('div');
            $starContainer.setAttribute('id',i+1);
            $star.classList.add('fa','fa-star','fa-2x');
            //adding to container
            $starContainer.appendChild($star);
            //registering the events to star-containers
            $starContainer.addEventListener("mouseenter",starEnterHandler.bind(null,i+1));
            $starContainer.addEventListener("mouseleave",starLeaveHandler);
            $starContainer.addEventListener("click",starClickedHandler);
            //appending the star container to main-component
            $ratingComponent.appendChild($starContainer);

        }
        function starEnterHandler($starContainerId)
        {

            $selectedStar = $starContainerId;
            if($isSelected)
            {
                //setting back the status
                $isSelected = false;
                document.querySelectorAll('.star-container').forEach($starContainer => {
                    $starContainer.addEventListener("mouseleave",starLeaveHandler);
                });
            }

            //removing colors from all stars
            for(i=1;i<=3;i++)
            {
                document.getElementById(i).classList.remove('text-warning');
                document.getElementById(i).classList.remove('text-success');
            }

            //default color
            $textColor = 'text-warning';
            //success color
            if($starContainerId === 3)
            $textColor = 'text-success';


            //adding colors to valid stars
            for(i=1;i<=$starContainerId;i++)
            {

                $star = document.getElementById(i);
                if(!$star.classList.contains('text-warning'))
                     $star.classList.add($textColor);
            }
        }

        function starLeaveHandler()
        {
            //removing colors from all stars
            for(i=1;i<=3;i++)
            {
                document.getElementById(i).classList.remove('text-warning');
                document.getElementById(i).classList.remove('text-success');
            }
        }

        function starClickedHandler()
        {
            //preserving the status for clicked
            $isSelected = true;

            //removing starLeave event to prevent the removing of colors
            document.querySelectorAll('.star-container').forEach($starContainer => {
            $starContainer.removeEventListener("mouseleave",starLeaveHandler);
            });


        }


</script>

<script>
    function acceptButtonHandler()
     {

        $form = document.getElementById('accept_form');
        $pointsInput = document.getElementById('input-points');
        $pointsInput.value = 1;
        if($isSelected)
        {
            $pointsInput.value = $selectedStar*3;
        }
        $form.submit();
     }


 </script>


@endsection


