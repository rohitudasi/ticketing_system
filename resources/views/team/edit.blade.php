@extends('layouts.app')
@section('page-title','Update Team')
@section('content')
<div class="row">
    <div class="mx-auto col-md-8">
    <form action="{{ route('team.update',$team) }}" method="POST">
        @method('PUT')
        @csrf

        <div class="form-group">
            <label for="name">Group Name</label>

        <input type="text" name="name" value="{{$team->name}}" class="form-control" >

        </div>


        <div class="form-group">
            <label for="sel1">Select Leader</label>
            <select class="form-control" onchange = "renderMembers(this.value,{{$users}},{{$team}})" name="team-leader">
            <option selected value="{{$team->leader->id}}">{{$team->leader->name}}</option>
                @foreach($users as $user)
              @if(!$user->team_id)
             <option value="{{$user->id}}">{{$user->name}}</option>
            @endif
             @endforeach
            </select>
          </div>


          <div class="form-group">
            <label for="sel2">Select Leader</label>
            <select id="sel2" class="form-control" name="team-members[]" multiple>
              @foreach($users as $user)
                @if($team->leader->id !== $user->id)
                 <option @if($user->team_id === $team->id)selected @endif value="{{$user->id}}">{{$user->name}}</option>
                @endif
                 @endforeach
            </select>
          </div>



        <button type="submit" class="btn btn-primary">Update</button>
    </form>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script>

    function renderMembers($leaderId,$users,$team)
    {
        $membersSelect = document.getElementById('sel2');
        $membersSelect.innerHTML = "";
        $users.forEach($user => {
            if($leaderId != $user.id){

            console.log($leaderId);
            $userOption = document.createElement('option');
            $userOption.setAttribute('value',$user.id);
            $userOption.innerHTML = $user.name;
            if($user.team_id === $team.id && $user.id !== $team.leader_id)
            {
                $userOption.setAttribute('selected','true');
            }
            $membersSelect.appendChild($userOption);
            }
        });

    }
</script>
@endsection
