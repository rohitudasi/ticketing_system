@extends('layouts.app')
@section('page-title','Create Team')
@section('content')
<div class="row">
    <div class="mx-auto col-md-8">
    <form action="{{ route('team.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Team Name</label>
              <input type="text" name="team-name" class="form-control"  placeholder="name">
        </div>

        <div class="form-group">
            <label for="sel1">Select Leader</label>
            <select class="form-control" onchange = "renderMembers(this.value,{{$users}})" name="team-leader">
                <option disabled selected value> -- select an leader -- </option>
                @foreach($users as $user)
             <option value="{{$user->id}}">{{$user->name}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="sel2">Select Leader</label>
            <select id="sel2" class="form-control" name="team-members[]" multiple>
              @foreach($users as $user)

                 <option value="{{$user->id}}">{{$user->name}}</option>
              @endforeach
            </select>
          </div>


        <button type="submit" class="btn btn-primary">Create</button>
    </form>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script>

    function renderMembers($leaderId,$users)
    {
        $membersSelect = document.getElementById('sel2');
        $membersSelect.innerHTML = "";
        $users.forEach($user => {
            if($leaderId != $user.id){
            $userOption = document.createElement('option');
            $userOption.setAttribute('value',$user.id);
            $userOption.innerHTML = $user.name;
            $membersSelect.appendChild($userOption);
            }
        });

    }
</script>
@endsection
