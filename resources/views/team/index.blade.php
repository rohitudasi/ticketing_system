@extends('layouts.app')
@section('page-title','Teams')
@section('content')

<div class="d-flex justify-content-end mb-3">
    <a class="btn btn-primary" href="{{route('team.create')}}">Add Team</a>
</div>
<table class="table table-bordered">
    <thead>
      <tr>

        <th scope="col">Name</th>
        <th scope="col">Leader</th>
        <th scope="col">Tasks</th>
        <th scope="col">Members</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
    @foreach($teams as $team)
      <tr>

        <td>{{$team->name}}</td>
        <td>{{$team->leader->name}}</td>
        <td>{{$team->tasks()->count()}}</td>
        <td>{{$team->members()->count()}}</td>
        <td>
            <a href="{{route('team.show',$team->id)}}" class="btn btn-primary">View</a>
            <a href="{{route('team.edit',$team->id)}}" class="btn btn-primary">Edit</a></td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endsection
