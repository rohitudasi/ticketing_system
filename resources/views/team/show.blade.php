@extends('layouts.app')
@section('page-title','Team Details')
@section('content')
<h1>Tasks</h1>
<div class="row">
    <div class="col-md-8">
        <table class="table table-bordered">
            <thead>
              <tr>

                <th scope="col">Task</th>
                <th scope="col">Priority</th>
                <th scope="col">Deadline</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
            @foreach($team->tasks as $task)
              <tr>

                <td>{{$task->title}}</td>
                <td>{{$task->priority}}</td>
                <td>{{$task->deadline}}</td>
                <td>{{$task->status}}</td>

              </tr>
            @endforeach
            </tbody>
          </table>

    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="d-flex justify-content-between">
            <h1>Members</h1>

        </div>
        <table class="table table-bordered">
            <thead>
              <tr>

                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Status</th>
                <th scope="col">Role</th>

              </tr>
            </thead>
            <tbody>
            @foreach($team->members as $member)
              <tr>

                <td>{{$member->name}}</td>
                <td>{{$member->email}}</td>
                <td>{{$member->status}}</td>
                 <td>{{$member->role}}</td>




              </tr>
            @endforeach
            </tbody>
          </table>

    </div>
</div>

@endsection
