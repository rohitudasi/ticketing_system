
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts._header')
    @yield('page-level-styles')
</head>
<body class="sb-nav-fixed">
    @include('layouts._navigation')
    <div id="layoutSidenav">
       @include('layouts._sidebar')
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">


                       @yield('page-header')

                 @yield('content')

                </div>
            </main>
           @include('layouts._footer')
        </div>
    </div>

    @include('layouts._scripts');

</body>
</html>
