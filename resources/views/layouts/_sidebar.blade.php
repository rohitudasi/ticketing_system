

<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="{{route('dashboard')}}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>

                <div class="sb-sidenav-menu-heading">Admin</div>

                @auth
                {{-- Teams --}}


                @php
                    $userRole = auth()->user()->role;
                @endphp

                @if($userRole === 'team-leader')
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#teamOption"
                    aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                            Team
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                     </a>

                     <div class="collapse" id="teamOption" aria-labelledby="headingOne"
                     data-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                           <a class="nav-link" href="{{route('team.show',auth()->user()->team_id)}}">View Team</a>
                        </nav>
                    </div>
                    <div class="collapse" id="teamOption" aria-labelledby="headingOne"
                    data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link" href="{{route('team.edit',auth()->user()->team_id)}}">Edit Team</a>

                    </nav>
                </div>


                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#taskOption"
                aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Tasks
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                 </a>

                 <div class="collapse" id="taskOption" aria-labelledby="headingOne"
                 data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                       <a class="nav-link" href="{{route('task.index')}}">View Tasks</a>
                    </nav>
                </div>

                @endif


                @if(auth()->user()->role === 'admin')
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts"
                aria-expanded="false" aria-controls="collapseLayouts">
                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                Teams
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne"
                    data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('team.index')}}">All Teams</a>
                    </nav>
                </div>
                @endif



                @if(auth()->user()->role === 'team-member')
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#memberTaskOptions"
                aria-expanded="false" aria-controls="collapseLayouts">
                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                     Tasks
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="memberTaskOptions" aria-labelledby="headingOne"
                    data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('task.index')}}">All Tasks</a>
                    </nav>
                </div>
                @endif



                @endauth
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            Start Bootstrap
        </div>
    </nav>
</div>
