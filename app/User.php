<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_login_at', 'last_login_ip', 'last_logout_at', 'total_login_time',
    ];

    protected $attributes = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Relationship functions:

    public function team()
    {
        return $this->belongsTo(Team::class);
    }
    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'users_tasks', 'user_id', 'task_id')
            ->withPivot('status', 'points');
    }
    public function task($id)
    {
        return $this->tasks()->where('task_id', $id);
    }

    public function assignedTasks()
    {
        return $this->belongsToMany(Task::class, 'users_tasks', 'user_id', 'task_id')
            ->wherePivot('status', 'assigned')
            ->get();
    }

    public function assignedTasksCount()
    {
        return $this->belongsToMany(Task::class, 'users_tasks', 'user_id', 'task_id')
            ->wherePivot('status', 'assigned')
            ->count();
    }
    public function underReviewTasks()
    {
        return $this->belongsToMany(Task::class, 'users_tasks', 'user_id', 'task_id')
            ->wherePivot('status', 'review')
            ->get();
    }
    public function underReviewTasksCount()
    {
        return $this->belongsToMany(Task::class, 'users_tasks', 'user_id', 'task_id')
            ->wherePivot('status', 'review')
            ->count();
    }
    public function completedTasks()
    {
        return $this->belongsToMany(Task::class, 'users_tasks', 'user_id', 'task_id')
            ->wherePivot('status', 'completed')
            ->get();
    }
    public function completedTasksCount()
    {
        return $this->belongsToMany(Task::class, 'users_tasks', 'user_id', 'task_id')
            ->wherePivot('status', 'completed')
            ->count();
    }
    public function failedTasks()
    {
        return $this->belongsToMany(Task::class, 'users_tasks', 'user_id', 'task_id')
            ->wherePivot('status', 'failed')
            ->get();
    }
    public function failedTasksCount()
    {
        return $this->belongsToMany(Task::class, 'users_tasks', 'user_id', 'task_id')
            ->wherePivot('status', 'failed')
            ->count();
    }


    public function updatePoints($points)
    {
        $this->points +=  $points;
        $this->save();
    }
    public function updateAssignedCount($count)
    {
        $this->assigned_tasks +=  $count;
        $this->save();
    }


    //helper functions:

    public function isAdmin()
    {
        return $this->role === 'admin';
    }

    public function isLeader()
    {
        return $this->role === 'team-leader';
    }

    public function isMember()
    {
        return $this->role === 'team-member';
    }
}
