<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
    protected $fillable = ['title', 'deadline', 'priority'];
    protected $dates = ['deadline'];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class, 'users_tasks', 'task_id', 'user_id')->withPivot('status', 'points');;
    }
    public function assignedTo()
    {

        return $this->users()->wherePivot('status', 'assigned')->first();
        // return DB::table('users_tasks')->where('task_id', $this->id)->where('status', 'assigned')->pluck('user_id')->first();
    }
    /**
     * ACCESSORS
     */

    public function makeLabel($color, $text)
    {
        return '<div class="d-flex ' . $color . ' justify-content-center align-items-center"><p class=" p-0 m-0  text-white">' .  $text . '</p></div>';
    }
    public function getPriorityAttribute($value)
    {

        if ($value === 2) {
            return $this->makeLabel('bg-danger', 'HIGH');
        }
        if ($value === 1) {
            return $this->makeLabel('bg-warning', 'MEDIUM');
        }
        if ($value === 0) {
            return $this->makeLabel('bg-primary', 'LOW');
        }
    }

    //helper classes

    public function status()
    {

        if (auth()->user()->role === 'team-member') {

            $status = $auth->user()->tasks()->where('task_id', $this->id)->first()->status;
            return $status;
        }

        if (auth()->user()->isLeader()) {
            if ($this->status === 'completed') {
                return 'completed';
            }

            if ($this->deadline < now()) {
                return 'failed';
            }
        }





        return $this->status;
    }

    public function statusColor()
    {

        $statusText = "text-secondary";

        if ($this->status === 'completed') {
            return 'text-success';
        }

        if ($this->status === 'failed') {
            return 'text-danger';
        }

        if ($this->status === 'assigned') {
            return "text-primary";
        }

        return $statusText;
    }


    public static function updateFailedTasks()
    {

        //tasks ids of the tasks whose status is 'assigned' or 'review'
        $tasksForCheck =   DB::table('users_tasks')
            ->whereNotIn('status', ['failed', 'completed'])
            ->pluck('task_id');

        $failedTasksIds = DB::table('tasks')
            ->whereIn('id', $tasksForCheck)
            ->where('deadline', '<', now())
            ->pluck('id');

        DB::table('users_tasks')
            ->whereIn('task_id', $failedTasksIds)
            ->update(['status' => 'failed']);


        $failedMembers =  DB::table('users_tasks')
            ->whereIn('task_id', $failedTasksIds)
            ->pluck('user_id');

        DB::table('users')
            ->whereIn('id', $failedMembers)
            ->increment('points', -5);

        DB::table('tasks')
            ->whereNotIn('status', ['completed', 'failed'])
            ->where('deadline', '<', now())
            ->update(['status' => 'failed']);
    }

    public function updateRecordStatus($memberId, $status)
    {
        DB::table('users_tasks')->where([['task_id', '=', $this->id], ['user_id', '=', $memberId]])->update(['status' => $status]);
    }
    public function updateRecordPoints($memberId, $points)
    {
        DB::table('users_tasks')->where([['task_id', '=', $this->id], ['user_id', '=', $memberId]])->update(['points' => $points]);
    }
}
