<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

    //relationship functions:

    public function leader()
    {
        return $this->hasOne(User::class, 'id', 'leader_id');
    }

    public function members()
    {

        return $this->hasMany(User::class)->where('role', 'team-member');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    public function actionTasks()
    {

        return $this->tasks()->whereIn('status', ['review', 'created'])->get();
    }
    public function bestMember()
    {
        $member =  User::where('team_id', $this->id)->where('role', 'team-member')->where('points', '>', 0)->orderByDesc('points')->first();

        if ($member) {
            return $member->name;
        } else {
            return 'Not Available';
        }
    }
}
