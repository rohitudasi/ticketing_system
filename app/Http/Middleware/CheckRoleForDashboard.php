<?php

namespace App\Http\Middleware;

use Closure;
use App\Task;
use Illuminate\Support\Facades\DB;



class CheckRoleForDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Task::updateFailedTasks();

        if ($request->user() && $request->user()->role === 'team-leader') {

            $member = $request->user();
            $tasks = $member->team->actionTasks();
            return response()->view('leaderDashboard', ['tasks' => $tasks, 'team' => $request->user()->team], 200);
        }
        if ($request->user() && $request->user()->role === 'team-member') {

            $member = $request->user();
            $tasks = $member->assignedTasks();
            return response()->view('memberDashboard', ['tasks' => $tasks, 'member' => $member], 200);
        }
        if (!request()->user()) {
            return redirect(route('login'));
        }

        return $next($request);
    }
}
