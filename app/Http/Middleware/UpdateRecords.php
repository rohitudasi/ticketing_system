<?php

namespace App\Http\Middleware;

use App\Task;
use Closure;

class UpdateRecords
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        Task::updateFailedTasks();
        return $next($request);
    }
}
