<?php

namespace App\Http\Controllers;

use App\Task;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TasksController extends Controller
{


    public function __construct()
    {
        $this->middleware('updateRecords')->only(['update', 'memberTasks']);
    }

    public function index()
    {
        $this->authorize('viewAny', new Task());
        return view('task.index')
            ->with(['tasks' => auth()->user()->team->tasks, 'team' => auth()->user()->team]);
    }

    public function memberTasks($status)
    {

        $member = auth()->user();

        if ($status === 'completed')
            return view('task.member.index')
                ->with(['tasks' => $member->completedTasks()]);

        if ($status === 'assigned')
            return view('task.member.index')
                ->with(['tasks' => $member->assignedTasks()]);

        if ($status === 'review')
            return view('task.member.index')
                ->with(['tasks' => $member->underReviewTasks()]);

        if ($status === 'failed')
            return view('task.member.index')
                ->with(['tasks' => $member->failedTasks()]);
    }

    public function create()
    {
        return view('task.create')
            ->with(['members' => auth()->user()->team->members]);
    }

    public function store(Request $request)
    {

        //setting title,priority and deadline with mass assignment
        $task = new Task(request()->all());


        $team = auth()->user()->team;
        $task->team_id = $team->id;
        $task->status = "created";
        // $task->save();


        if (request('is-automatic') === 'true') {

            //if priority of the task is highest then task should be assigned to only best member


            //check whether any vacant user exists and if exists more than 1
            // then the task will be assigned to the user which has highest points
            $member = $team->members()->where('assigned_tasks', 0)->orderByDesc('points')->orderByDesc('total_login_time')->first();

            if ($member) {

                dd($member->id);
                //assign the task to that member that is best in vacant

            } else {
                $member = User::all()->orderByDesc('points')->orderByDesc('total_login_time')->orderBydesc('assigned_tasks')->first();
                dd($member->Id);
                //no vacant user is found
                //assing the task to the user who has less no of assigned tasks



            }


            //if no vacant user is available
            //assign the task the user who has less numbers of tasks


        }

        $memberId = request('member');
        $member = User::find($memberId);
        $task->status = "assigned";
        $task->save();
        $member->tasks()->attach([$task->id], ['status' => 'assigned']);
        $member->updateAssignedCount(1);
        return redirect(route('task.index'));
    }

    public function edit(Task $task)
    {
        return view('task.create')
            ->with(['members' => auth()->user()->team->members]);
    }

    public function assign(Task $task)
    {
        return view('task.assign')
            ->with([
                'task' => $task,
                'members' => auth()->user()->team->members
            ]);
    }


    public function update(Request $request, Task $task)
    {


        if (request('is_task_assign'))
            return $this->assignTask($task);

        if (request('is_task_submit'))
            return $this->submitTask($task);

        if (request('is_task_giveup'))
            return $this->giveUpTask($task);

        if (request('is_task_accept'))
            return $this->acceptTask($task);

        if (request('is_task_reassign'))
            return $this->reAssignTask($task);
    }




    /****************************HELPER FUNCTIONS********************************** */

    private function assignTask($task)
    {
        $task->priority = request('task-priority');
        $task->deadline = request('task-deadline');
        $task->status = "created";
        $task->save();
        $memberId = request('task-member');
        $member = User::find($memberId);
        $member->updateAssignedCount(1);
        $alreadyExistRecord = $member->task($task->id)->first();
        if ($alreadyExistRecord) {

            //record already exists in users_tasks of user with this task
            // we donot need to create new record
            // we only have to update the status

            $task->updateRecordStatus($memberId, 'assigned');
        } else {

            $member->tasks()->attach([$task->id], ['status' => 'assigned']);
        }
        $task->status = "assigned";
        $task->save();
        return redirect(route('dashboard'));
    }


    private function submitTask($task)
    {
        $task->status = "review";
        $task->save();
        $memberId = request('task_member');
        $member = User::find($memberId);
        $member->updateAssignedCount(-1);
        $task->updateRecordStatus($memberId, 'review');
        return redirect(route('dashboard'));
    }

    private function giveUpTask($task)
    {
        $task->status = "created";
        $task->save();
        $memberId = request('task_member');
        $member = User::find($memberId);
        $member->updateAssignedCount(-1);
        $member->updatePoints(-3);
        $task->updateRecordStatus($memberId, 'failed');
        return redirect(route('dashboard'));
    }

    private function acceptTask($task)
    {

        $task->status = "completed";
        $task->save();
        $memberId = DB::table('users_tasks')
            ->where('task_id', $task->id)
            ->where('status', 'review')
            ->pluck('user_id')
            ->first();
        $member = User::find($memberId);
        $points = request('task_points');
        $member->updatePoints($points);
        $task->updateRecordStatus($memberId, 'completed');
        $task->updateRecordPoints($memberId, $points);
        $task->save();
        return redirect(route('dashboard'));
    }

    private function reAssignTask($task)
    {
        $task->status = "assigned";
        $task->save();
        $memberId = DB::table('users_tasks')
            ->where('task_id', $task->id)
            ->where('status', 'review')
            ->pluck('user_id');
        $member = User::find($memberId)->first();
        $member->updateAssignedCount(+1);
        $member->updatePoints(-1);
        $task->updateRecordStatus($memberId, 'assigned');
        return redirect(route('dashboard'));
    }
}
