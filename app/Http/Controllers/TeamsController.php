<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Team;
use App\User;

class TeamsController extends Controller
{
    //
    public function index()
    {

        $this->authorize('index', Team::class);
        $teams = Team::all();
        return view('team.index')->with(['teams' => $teams]);
    }

    public function create()
    {

        $this->authorize('create', Team::class);
        $users = User::whereNull('team_id')->get();

        return view('team.create')->with(['users' => $users]);
    }

    public function store()
    {

        $this->authorize('create', Team::class);
        $team = new Team();
        $team->name = request('team-name');
        $leader = User::where('id', request('team-leader'))->first();
        $leader->password = Hash::make('luvmylife');
        $leader->status = "vacant";
        $leader->role = "team-leader";
        $team->leader_id = $leader->id;
        $team->save();

        $members = request('team-members');
        for ($i = 0; $i < count($members); $i++) {
            $member = User::where('id', $members[$i])->first();
            $team->members()->save($member);
        }
        $leader->team_id = $team->id;
        $leader->Save();
        return redirect(route('team.index'));
    }

    public function show(Team $team)
    {
        $this->authorize('view', $team);
        return view('team.show')->with(['team' => $team]);
    }
    public function edit(Team $team)
    {
        $users = User::whereNull('team_id')->orWhere('team_id', $team->id)->get();
        return view('team.edit')->with(['team' => $team, 'users' => $users]);
    }
    public function update(Request $request, $oldTeamId)
    {


        $oldTeam = Team::findOrFail($oldTeamId);
        $oldMembers =  $oldTeam->members;
        $newMembers = request('team-members');

        //removing all the old members

        for ($i = 0; $i < count($oldMembers); $i++) {
            $oldMembers[$i]->team_id = null;
            $oldMembers[$i]->role = 'team-member';
            $oldMembers[$i]->save();
        }



        //adding new members:
        for ($i = 0; $i < count($newMembers); $i++) {
            $newMember = User::findOrFail($newMembers[$i]);
            $oldTeam->members()->save($newMember);
        }




        //adding Leader of team

        $oldTeam->leader_id = null;
        $teamLeader = User::findOrFail(request('team-leader'));
        $teamLeader->role = "team-leader";
        $teamLeader->team_id = $oldTeam->id;
        $teamLeader->save();
        $oldTeam->leader_id = $teamLeader->id;




        //updating the name:
        $oldTeam->name = request('name');

        //saving the team
        $oldTeam->save();

        return redirect(route('team.index'));
    }
}
