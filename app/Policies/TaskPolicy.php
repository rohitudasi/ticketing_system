<?php

namespace App\Policies;

use App\Task;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {

        return ($user->isLeader() || $user->isAdmin());
    }



    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function view(User $user, Task $task)
    {
        //

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function update(User $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function delete(User $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function restore(User $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function forceDelete(User $user, Task $task)
    {
        //
    }

    public function applyActions(User $user, Task $task)
    {
        if ($task->status === 'completed') {
            return false;
        }
        if ($task->deadline < now()) {
            return false;
        }
        if (($task->status === 'review')) {
            return false;
        }
        if (($task->status === 'created')) {
            return false;
        }

        return true;
    }

    public function acceptOrReject(User $user, Task $task)
    {
        if ($user->isLeader()) {
            return (($task->status == 'review')  && ($task->deadline > now()));
        }
    }
    public function assign(User $user, Task $task)
    {
        if ($user->isLeader()) {
            return (($task->status === 'created') && ($task->deadline > now()));
        }
    }
}
