<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        //     factory(\App\User::class, 5)->create();

        //     factory(\App\Team::class, 5)->create()
        //         ->each(function ($team) {
        //             $leader = User::whereNull('team_id')->first();
        //             $leader->role = "team-leader";
        //             $team->leader()->save($leader);
        //             $team->leader_id = $leader->id;
        //             $team->members()->saveMany(factory(\App\User::class, 10)->make());
        //             $team->save();
        //         });
        // }
        $admin = new User();
        $admin->name = "Rohit udasi";
        $admin->email = "rohitudasi44@gmail.com";
        $admin->role = 'admin';
        $admin->team_id = 0;
        $admin->password = Hash::make('luvmylife');
        $admin->save();




        factory(\App\User::class, 5)->create()
            ->each(function ($leader) {
                $team = factory(\App\Team::class, 1)->create()->first();
                $leader->role = "team-leader";
                $leader->team_id = $team->id;
                $leader->password = Hash::make('luvmylife');
                $team->leader_id = $leader->id;
                $leader->save();
                $team->members()->saveMany(factory(\App\User::class, 10)->make());
                $team->tasks()->saveMany(factory(\App\Task::class, 5)->make());
                $team->save();
            });
    }
}
