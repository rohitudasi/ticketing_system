<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        //
        'status' => 'created',
        'created_at' => now(),
        'title' => $faker->sentence(),
        'priority' => 0,
        'deadline' => now()->addDays(2)->addMinutes(rand(1, 55)),

    ];
});
