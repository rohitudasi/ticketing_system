<?php

use App\Http\Controllers\TasksController;
use Illuminate\Support\Facades\Route;
use App\Task;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('admin/welcome');
// });
// Route::get('/', function () {
//     return view
// }


Route::resource('team', 'TeamsController')->middleware('auth');
Route::resource('task', 'TasksController')->middleware('auth');
Route::resource('users', 'UsersController')->middleware('auth');

Route::get('task/{task}/assign', 'TasksController@assign')->name('task.assign');
Auth::routes();
Route::get('task/show/{status}', 'TasksController@memberTasks')->name('task.membertasks');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/', function () {

    // if (auth()->user()) {

    //     $user = auth()->user();
    //     if ($user->isLeader()) {
    //         $tasks = Task::where('team_id', $user->team_id)->get();
    //         return view('leaderDashboard')->with(['tasks' => $tasks]);
    //     }
    //     if ($user->isMember()) {
    //         $taskIds = DB::table('users_tasks')->where('user_id', 9)->pluck('task_id');
    //         $tasks = Task::whereIn('id', $taskIds)->get();
    //         return view('memberDashboard')->with(['tasks' => $tasks]);
    //     }
    // } else {
    //     return redirect(route('login'));
    // }
})->name('dashboard')->middleware('roleForDashboard');
